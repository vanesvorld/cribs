# Crucible Reviewer Crib

## Review Workflow

* `j` and `k` navigate through diffs and comments.
* 'r' reply to a comment
* 'm' make comment unread



## Miscellaneous

* `.` open action dialog

