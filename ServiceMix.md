# Servicemix Crib

# Startup

* Start SM:  `servicemix start`
    * with debug on 5005:  `servicemix start debug`

# Console

* List loaded bundles: `bundle:list`
