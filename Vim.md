# John's Neovim Cheatsheet

### Buffers
  * `<F11>` buffer previous
  * `<F12>` buffer next
  * `<F10>` close buffer
  * `<F9>` toggle Buffergator (`,bl`)

### Modes
  * `<F2>` Toggle fold
  * `<F3>` Toggle PASTE mode
  * `<F4>` (With visual) copy visual to system clipoard
  * `<F5>` New buffer

### Opening and Buffer Management
  * `<CTRL-P>` smart search with open
  * `,bl` Buffergator - list/open
  
### Sessions

  * `:mks <file>` write a session file
  * `:source <file>` restore a session from file
  * `nvim -S <file>` start vi with the given session

### Useful Command-Mode Commands

  * `:w !markdown|bcat` pipe current buffer through markdown and display in browser (bcat is a rubygem)
  * `:sort` sort in place (Simple ascii)
  * `:%!cmd` filter buffer through `cmd` and replace with result
  * `:%s/foo/bar/g` foo => bar, all lines, all occurrences

### Terminal

  * `:terminal` start terminal
  * `<C-\><C-N>` return to Normal

### Vim-Eunoch

  * `SudoEdit` edit a file with sudo
  * `SudoWrite` write a file with sudo
