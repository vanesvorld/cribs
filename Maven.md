# Maven Crib

## PAX-Exam (Integration Test)

- Run all integration tests in Failsafe: 
    - `mvn clean verify`
- Run a single test:
    - `mvn clean verify -Dit.test=net.hawksley.awesomeproject.test.SomePaxTest`
- Debug with suspend:
    - `-Dpax.debug`
    - `-Dpax.debug.port=5005`
- Verbose PAX output:
    - `-Dpax.verbose`
- Don't clean up PAX folder after test:
    - `-Dpax.no.clean.up`

