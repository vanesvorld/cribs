# Kubernetes


## QRC

### Minikube

* Minikube (VM environment for Kubernetes)
  * `minikube dashboard` - launch a Dashboard server for the cluster
  * `minikube service list` - get a list of services available on the cluster
  * `minikube service my-service` - fire URL for `my-service` in browser
    * `--url` - just display it
  * `minikube start|stop|delete` - start, stop and remove the MK VM.

### Mounting Local Folder to MK VM

Mount a local folder to the VM allows containers to mount them.

```
minikube mount ~/local/folder:/tmp/mount/inside/vm
```

### Kubectl

* Kubectl (control the cluster)
  * `kubectl get all` - get full status of resources
  * `kubectl delete service/myservice` - delete a resource
  * `kubectl apply -f ./somefile.yaml` - apply a YAML resource file to the cluster


## Links

* [Kubernetes Home](https://kubernetes.io)
* [Kubernetes Documentation](https://kubernetes.io/docs/home/)
* [Kubernetes Networking](https://medium.com/google-cloud/kubernetes-nodeport-vs-loadbalancer-vs-ingress-when-should-i-use-what-922f010849e0) - Explained!

