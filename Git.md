# Git / Gitlab QRC

# Useful URLs

* [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/)
* [Atlassian - Feature Branch Git Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow)
* [Git Markdown Reference](https://guides.github.com/features/mastering-markdown/)

# Tagging

## Create a Tag
- Obtain tree ID (commit ID)
  - `git log --pretty=oneline` 
  - `git --no-pager log --oneline --decorate --graph --color`
  - (My env:) `glog`
- Use that to tag
  - `git tag -a v1.2 9fceb02`
  - An annotated tag:
    - `git tag -a <tag-name> -m <tag-message>`
- Push the tag (not done by default)
  - `git push origin --tags`

## Delete a Tag
- `git tag -d v1.4-lw`
- Push the deletion
  - `git push origin :refs/tags/v1.4-lw`

## List Tags
- `git tag`
- With annotations
  - `git tag -l -n9`
  - (My Env:) `gtagl`


# Workflow

This workflow reflects the master-branch model [here](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow)

Assumes a clone of the origin/master has already been done.

## Start with master branch

```
git checkout master
git fetch origin
git reset --hard origin/master
```

## Create a new new branch

```
git checkout -b new-feature
```

## Work

Perform the usual add, commit work on this new branch.

## Push this new branch to remote

```
git push -u origin new-feature
```

## Repeat from Work.

## File PR

In Gitlab:

* `Repository > Branches' select `Merge Request`
* Fill out MR.  Add WIP if still working. Perform more commits if necessary.  Set:
  * Squash commits
  * Delete branch
* When done:
  * On Gitlab, merge the MR.

Optionally:

* Return to the master:
  * `git checkout master`
* .. and pull.
  * `git pull`



# Tag a Release

Do the tag locally against a commit ref:

```
git tag -a v1.2.0 9fceb02
```

Push all tags to origin:

```
git push origin --tags
```


# Working on a fork

## Updating your clone from upstream

Add the upstream:

```
git remote add upstream https://github.com/whoever/whatever.git
```

Fetch all branches of that upstream into remote-tracking branches, 
such as upstream/master:

```
git fetch upstream
```

Switch to locate master branch:

```
git checkout master
```

Rewrite your master branch so that any commits of yours that aren't
already in upstream/master are replayed on top of that
other branch

```
git rebase upstream/master
```


# Useful Commands

* Find branches that have not been merged into master

```
git branch --no-merged master
```

If you've rebased your branch onto upstream/master you may need to force the push in order to push it to your own forked repository on GitHub. You'd do that with:

```
git push -f origin master
```
