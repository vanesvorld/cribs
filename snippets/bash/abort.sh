# Function to abort in a pretty way if the passed param isn't 0
# Can be used to check $? for every command.

function test_and_abort()
{
    ERR=$1
    ABORT_CODE=$2
    MESSAGE=$3

    if [ ${ERR} != 0 ]; then
        printf " ** ABORT ** ${MESSAGE}\n"
        exit ${ABORT_CODE}
    fi
}

test_and_abort -2 -5 "Couldn't foo and bar."
