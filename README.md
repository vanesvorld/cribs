# Cribs 
*A collection of more or less useful crib sheets*

## Languages

* [Ruby](Ruby.md)
* Java
    * [Maven](Maven.md)
    * [Servicemix](ServiceMix.md)
* [Python](Python.md)

## Applications

* [Vim](Vim.md)
* [Crucible](Crucible.md)
* [Git](Git.md)

## Containers

* [Kubernetes](Kubernetes.md)

## Environments

* [Unix](Unix.md)

