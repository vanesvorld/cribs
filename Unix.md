# Unix General

## AWS Linux 

 To keep EC2 instances from timing out SSH sessions:
 
On the Server:

```
    echo 'ClientAliveInterval 60' | sudo tee --append /etc/ssh/sshd_config
    sudo service sshd restart
```

On the client

```
    in ~/.ssh/config
        ServerAliveInterval 50
```
