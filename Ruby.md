# Ruby Crib

## Building Ruby Apps

- `rvm list [known]` - list installed/installable Rubies
- `rvm use x@y [--default]` - use ruby x with gemset y (and make default)
- `rvm gemset create|use|delete|list` - Gemset CRUD
- `rvm install x` - install ruby 'x'
- `gem list` - list gems
- `gem install|uninstall x` - gem CRUD
- `bundle gem x` - create template application 'x'
    - `rvm --ruby-version use 2.4.0@notebook-dev --create` -- set up .ruby-version and .ruby-gemset
- `bundle install` - install gems from Gemfile
- `bundle exec bin/foo` - run script `bin/foo` with the environment of this bundle.
- `rake build` - build the gem
- `rake install` - Package and install gem
- `gem push pkg/somegem-1.0.1.gem` - push built gem up to rubygems.

## Useful Gems

### Standalone Programs

- [RuboCop](https://github.com/bbatsov/rubocop) - Style checker based on the [Ruby Style Guide](https://github.com/bbatsov/ruby-style-guide) and its [documentation](http://rubocop.readthedocs.io/en/latest/).

### Libraries

- [SQLite3](https://github.com/sparklemotion/sqlite3-ruby/) - embedded database [FAQ](https://github.com/sparklemotion/sqlite3-ruby/blob/master/faq/faq.md) | [Gem](http://www.rubydoc.info/gems/sqlite3/SQLite3/)
- [Commander](https://github.com/commander-rb/commander) - Command-Line options parser | [RDoc](http://www.rubydoc.info/github/tj/commander/)
- Ruby-Terminfo - Read the terminal size (amongst other things) | [Gem](https://rubygems.org/gems/ruby-terminfo)
- [Paint](https://github.com/janlelis/paint) - Colourised output for Ruby programmes | [Gem](https://rubygems.org/gems/paint)
- [Open4](https://github.com/ahoward/open4) - Open subprocesses in Ruby with handles to pid, stdin, stdout, stderr | [Gem](https://rubygems.org/gems/open4/versions/1.3.4)
- [Require-All](https://github.com/jarmo/require_all) - Simplify `require` statements into one intelligent operation | [Gem]( https://rubygems.org/gems/require_all)
- [json](http://flori.github.com/json/) - Handle JSON files within Ruby | [Gem](https://rubygems.org/gems/json)
- [text-table](https://github.com/aptinio/text-table) - Draw ASCII tables comfortably in the terminal | [Gem](https://rubygems.org/gems/text-table)
- [ruby-progressbar](https://github.com/jfelchner/ruby-progressbar) - Very nice progress bar implementation | [Gem](https://rubygems.org/gems/ruby-progressbar)

## Documenting Ruby with YARD

- [YARD](https://github.com/lsegal/yard) - YARD tool
- [YARD cheatsheet](https://gist.github.com/chetan/1827484) - One of everything in YARD.
- [YARD Getting Started](http://www.rubydoc.info/gems/yard/file/docs/GettingStarted.md) - YARD's own Getting Started text.
- `yard server -r` - run YARD server, watching sources and reloading where necessary

## Useful Links

- [The Ruby Style Guide](https://github.com/bbatsov/ruby-style-guide)
- [RubyGems](https://rubygems.org/) [(and mine)](https://rubygems.org/profiles/jhawksley)
- [Travis CI](https://www.travis-ci.com/): Continuous integration - free for open source projects.
- [RSpec](http://rspec.info/) - Behaviour-driven testing for Ruby programmes.
- [Using Rdoc](http://www.mikeperham.com/2010/12/16/using-rdoc/) - Great quick intro to using RDoc.
- [SysV Init Template](https://github.com/fhd/init-script-template) - System V Init Script Template
